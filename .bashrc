if env | grep -q ^PKG_CONFIG_PATH=
then
    export PKG_CONFIG_PATH=$HOME/include/include/lib/pkgconfig/:$PKG_CONFIG_PATH
else
    export PKG_CONFIG_PATH=$HOME/include/include/lib/pkgconfig/
fi

if env | grep -q ^LD_LIBRARY_PATH=
then
    export LD_LIBRARY_PATH=$HOME/include/include/lib/:$LD_LIBRARY_PATH
else
    export LD_LIBRARY_PATH=$HOME/include/include/lib/
fi

export PATH=$HOME/include/include/include/:$HOME/include/include/lib/:$PATH

stty erase ^?
#alias emacs='emacs -nw'
