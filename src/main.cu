#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"
#include <string>
//#include "opencv2/gpu/gpu.hpp"
#include "stereomatch.hpp"
#include "disparity.hpp"
#include "lib.hpp"
#include <fstream>
#define PARAMETERS_FILENAME "parameters.param"
#include<sstream>

using namespace cv;
using namespace std;

extern int interestThreshold, numIterations;
char* image_directory;

int main(int argc, char** argv){

  image_directory = argv[3];
  cout<<"Image Directory "<<image_directory<<endl;
	if(argc!=4){
		std::cout<<"Usage: <program> <image-A> <image-B> <output-image-folder>"<<std::endl;
		return -1;
	}
	
	cv::Mat imgA, imgB;
	imgA = cv::imread(argv[1], CV_LOAD_IMAGE_ANYDEPTH);
	imgB = cv::imread(argv[2], CV_LOAD_IMAGE_ANYDEPTH);

	if(!imgA.data || !imgB.data){
		std::cout<<"Could not open the images"<<std::endl;
		return -1;
	}

	cv::Mat outputImg;
  
  ifstream fin(PARAMETERS_FILENAME);
  //int device=3, interestThreshold = 10, numIterations = 100;
  
  int device;
  string parameterName;
  fin>>parameterName>>device;
  fin>>parameterName>>interestThreshold;
  fin>>parameterName>>numIterations;
  
  cout<<"device no "<<device<<" interestThreshold "<<interestThreshold<<"  numIterations "<<numIterations<<endl;
  //cout<<"Enter device: "<<endl;
  //cin>>device;
  gpuErrorCheck(cudaSetDevice(device));
	
  timespec timeStart, timeEnd;  
  
  if(PROFILING){
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeStart);
  }
  
  generateModel( imgA, imgB, outputImg);
  
  if(PROFILING){
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
    timespec delta = diff(timeStart, timeEnd);
    cout<<"Overall model generated in "<<delta.tv_sec<<":"<<delta.tv_nsec<<endl;
  }

	cout<<"Writing model"<<endl;
	ostringstream filename;
  filename<<image_directory;
	filename<<argv[3]<<"s"<<STRIDE<<"w"<<W_SIZE<<"i"<<numIterations<<"sc"<<STEP_SCALE<<"t"<<interestThreshold<<".jpg";
  cv::imwrite(filename.str(), outputImg);

	return 0;
}
