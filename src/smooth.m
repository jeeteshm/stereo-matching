%%# Read an image
I = imread('data3.jpg');
%# Create the gaussian filter with hsize = [5 5] and sigma = 2
G = fspecial('gaussian',[10 10],30);
%# Filter it
Ig = imfilter(I,G,'same');
%# Display
%imshow(Ig)
Igt=double(Ig);
Igt=Igt(1:2000,2000:-1:1);
Igr=Igt(1:2:2000,1:2:2000);
mesh(Igr);
figure(2);
imshow(Ig);