#include "awt.hpp"
#include <sstream>
#include "lib.hpp"

extern char* image_directory;
const int filter_size = 3;
double filter_coeff[3][3] = 
  {
    {0.0625, 0.125, 0.0625},  //1/16, 1/8, 1/16
    {0.125, 0.25, 0.125},     //1/8, 1/4, 1/8
    {0.0625, 0.125, 0.0625}   //1/16, 1/8, 1/16
  };

Mat filter = Mat(filter_size, filter_size, CV_64F, filter_coeff);

void AWTCoeffGenerator::getNextAWTCoefficient(Mat& coeff){
  assert(level >= 0);

  int Size = image.rows;

  cout<<"Level: "<<level<<endl;
  Mat awt_cur;//downsample
  int cur_size;
    
  if(level>0){
    Mat ds;
    int step = (1 << level);
    cur_size = (Size >> level);
    //if(Size % (1 << level) !=0) cur_size++;

    ds.create(cur_size, cur_size, CV_8U);
    cout<<"cur_size: "<<cur_size<<endl;
    uint8_t* data = (uint8_t*) image.data;
    uint8_t* data_ds = (uint8_t*) ds.data;

    for(int iA=0, i=0; iA+step < Size; iA+= step, i++){
      for(int jA=0, j=0; jA+step < Size; jA+= step, j++){  
        /*cout<<"Size "<<Size<<" step "<<step<<" brdr "<<brdr<<" iA: "
          <<iA<<" jA: "<<jA<<" i: "<<i<<" j: "<<j<<endl;*/
        data_ds[i*cur_size + j] = data[iA*Size + jA];
      }
    } 

    awt_cur.create(cur_size, cur_size, CV_8U);      
    filter2D(ds, awt_cur, -1, filter);
    this->last_ds = ds;

  } else if (level == 0) {
    awt_cur = image;
    cur_size = Size;
    cout<<"cur_size: "<<cur_size<<endl;
    this->last_ds = image;
  } else {
    //level < 0 not allowed
    exit(-1);
  }

  /*
  ostringstream filename;
  filename << "awt_" << level << ".jpg";
  cout<<"Writing the AWT for level "<<level<<endl;
  imwrite(filename.str(),awt_cur);
  */

  if(!awt_prev.empty()){
    coeff.create(cur_size, cur_size, CV_8U);
    uint8_t *cdata = (uint8_t*)coeff.data;
    uint8_t *cur_data = awt_cur.data, *prev_data = awt_prev.data;

    //assert(awt_prev.rows*2 == awt_cur_size);

    for(int i=0; i<cur_size; i++){
      for(int j=0; j<cur_size; j++){
        int diff = prev_data[(i/2)*prev_size + (j/2)]
          - cur_data[i*cur_size + j];
        if(diff < 0) diff = -diff;
        cdata[i*cur_size + j] = diff;
      }
    }
      
    /*
    ostringstream filename;
    filename<<image_directory<<"coeff_"<<level+1<<".jpg";
    cout<<"Writing wavelet coefficients for level "<<level<<endl;
    imwrite(filename.str(), coeff);
    */
  }

  awt_prev = awt_cur;
  prev_size = cur_size;
  level --;

}

