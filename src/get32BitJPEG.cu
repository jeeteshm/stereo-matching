#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char** argv){
	
	if(argc!=3){
		cout<<"Usage: <input-image> <output-image>"<<endl;
		return -1;
	}
	
	cv::Mat inImg, outImg;
  inImg = cv::imread(argv[1], CV_LOAD_IMAGE_ANYDEPTH);

	if(!inImg.data){
		cout<<"Could not open the images"<<endl;
		return -1;
	}

  outImg.create(inImg.rows, inImg.cols, CV_32S);

	uint8_t* idata = (uint8_t*)inImg.data;
	uint32_t* odata = (uint32_t*)outImg.data;
	for(int i=0; i<inImg.rows; i++){
		for(int j=0; j<inImg.cols; j++){
      uint32_t intensity = idata[i*inImg.channels()*inImg.cols + j];
      intensity <<= 3;
			odata[i*outImg.channels()*outImg.cols + j] = intensity;
		}
	}		

	cv::imwrite(argv[2], outImg);
	return 0;
}
