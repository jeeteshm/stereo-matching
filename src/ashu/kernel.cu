#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"
#include <string>

//using namespace cv;
//using namespace std;

int main(int argc, char** argv){
	
	if(argc!=4){
		std::cout<<"Usage: <program> <image-A> <image-B> <output-image>"<<std::endl;
		return -1;
	}
	
	cv::Mat img;
	img = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);

	if(!img.data){
		std::cout<<"Could not open the image"<<std::endl;
		return -1;
	}

	cv::Mat grayImg;
	cv::cvtColor( img, grayImg, CV_BGR2GRAY );
	cv::imwrite(argv[3], grayImg);

	return 0;
}
