#include <stdio.h>
#include<math.h>
#include <cuda_runtime.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/gpu/gpu.hpp"
#define B_SIZE 16
#define W_SIZE 10
#define STRIDE 30
#define EPSILON 0.001
#define COEFF_RANGE 1000
#include <cstdlib>
#include <time.h>


using namespace std;
using namespace cv;

class BlockDisparity{
  //approximates local surface in a block of size BLOCK_SIZE
  //at coordinate (x,y)
  //by polynomial ax2 + by2 + cxy, dx, ey, f
public:
  bool filled;
  int x,y;
  double a, b, c, d, e, f;
};

class BlockDisparities{
  //array storing Disparity polynomials
public:
  int size;				//size = number of blocks
  BlockDisparity* block_disps;

  BlockDisparities(){
    this->block_disps = NULL;
  }

  ~BlockDisparities(){
    if(block_disps!=NULL){
      free(block_disps);
    }
  }
};

__device__ uint8_t find_min_disp(int r,int c, uint8_t *ia, uint8_t *ib, int imgsize )
{
	//int rmin=r;
	int cmin=c;
	unsigned int dmin=10000000,sum;
		
	for(int c2=c-STRIDE ; c2<= c+STRIDE;c2++)
	{
		sum=0;
		if(c2<0 || c2>=imgsize){continue;}
		for(int i=r-W_SIZE; i<=r+W_SIZE;i++)
		{
			if(i<0 || i>=imgsize){continue;}
			for(int j=c2-W_SIZE;j<=c2+W_SIZE;j++)
			{
				if(j<0 || j>=imgsize || j-c2+c <0 || j-c2+c>=imgsize ){continue;}
				{
					
					sum=sum+abs(ia[i*imgsize+j-c2+c]-ib[i*imgsize+j]);
				}
			}
		}
		if(sum<dmin){dmin=sum;cmin=c2;}
		
	}
	return (uint8_t)abs(c-cmin);		
}

__global__ void disparity_kernel(uint8_t *ca, uint8_t *ia, uint8_t *ib, uint8_t *id, int imgsize, int threshold, double* rand, BlockDisparity* bd )
{
    __shared__ int count;
    __shared__ int sh_A[256][6];
    __shared__ int sh_b[256]; 
    __shared__ int sh_ATA[6][6];
    __shared__ int sh_ATb[6];
    __shared__ double sh_xk1[6], sh_xk[6], diff;  
	
    if(threadIdx.x==0 && threadIdx.y==0)
    {
	count=0;
    }
    __syncthreads();
    int idx=0;
    int row=(blockIdx.y*blockDim.y)+threadIdx.y;
    int col=(blockIdx.x*blockDim.x)+threadIdx.x;
    int i=row*imgsize+col;
    if(i<imgsize*imgsize)
    { 
	if(ca[i]>threshold){
		id[i]=find_min_disp(row,col,ia,ib,imgsize);
		idx=atomicAdd(&count,1);
		sh_A[idx][0]=row*row;
		sh_A[idx][1]=col*col;
		sh_A[idx][2]=row*col;
		sh_A[idx][3]=row;
		sh_A[idx][4]=col;
		sh_A[idx][5]=1;
		sh_b[idx]=id[i]; 
		}
	//if(ca[i]>threshold){id[i]=255;}
    	else{id[i]=0;}
    }

    __syncthreads();
    
	if(count == 0){
		bd[gridDim.y * blockIdx.y + blockIdx.x].filled = false;
		return;
	}    
    
    if(threadIdx.x < 6 && threadIdx.y < 6){
      
      sh_ATA[threadIdx.x][threadIdx.y] = 0;
      for(int i = 0; i < count; i++){
	sh_ATA[threadIdx.x][threadIdx.y] += sh_A[i][threadIdx.x] * sh_A[i][threadIdx.y];
      }
    }
	
    if(threadIdx.x < 6 && threadIdx.y == 0){
      sh_ATb[threadIdx.x] = 0;
      for(int i=0; i<count; i++){
	sh_ATb[threadIdx.x] += sh_A[i][threadIdx.x] * sh_b[i];
      }
    }

    __syncthreads();

   if(threadIdx.x == 0 && threadIdx.y == 0){
	for(int i=0; i<6; i++){
		sh_xk[i] = rand[i];		
	}
	int it = 1;
	do{
		for(int i =0; i<6; i++){
			sh_xk1[i] = -sh_ATb[i];//x_k+1 = -A'b
			for(int j=0; j<6; j++){
				sh_xk1[i] += sh_ATA[i][j] * sh_xk[j];
			}
			sh_xk1[i] *= 0.01 * (1/(double)it);//x_k+1 = \gamma (A'Ax - A'b)
			sh_xk1[i] = sh_xk[i] - sh_xk1[i];//x_k+1 = x_k - \gamma (A'Ax - A'b)
		}
    it++;
	}while(it < 1000);

  bd[gridDim.y * blockIdx.y + blockIdx.x].filled = true;
	bd[gridDim.y * blockIdx.y + blockIdx.x].a = sh_xk1[0];
	bd[gridDim.y * blockIdx.y + blockIdx.x].b = sh_xk1[1];
	bd[gridDim.y * blockIdx.y + blockIdx.x].c = sh_xk1[2];
	bd[gridDim.y * blockIdx.y + blockIdx.x].d = sh_xk1[3];
	bd[gridDim.y * blockIdx.y + blockIdx.x].e = sh_xk1[4];
	bd[gridDim.y * blockIdx.y + blockIdx.x].f = sh_xk1[5];   
  }
}

BlockDisparity* getBlockDisparities(Mat imageA, Mat coeffA, Mat imageB){
	
	cudaError_t err = cudaSuccess;
	err=cudaSetDevice(3);
	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to set Device %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	int threshold=5;
	//cin>>threshold;
	int imgsize=(imageA.rows);
	//cout <<imgsize<<endl;
	size_t sizebyte = imgsize*imgsize*sizeof(uint8_t);
	//cout <<sizebyte<<endl;
	uint8_t* h_ca = (uint8_t*) coeffA.data;
	uint8_t* h_ia = (uint8_t*) imageA.data;
	uint8_t* h_ib = (uint8_t*) imageB.data;
	uint8_t* h_id = (uint8_t*) malloc(sizebyte);
	uint8_t *d_ca = NULL;
	double h_r[6], *d_r = NULL;
    	err = cudaMalloc((void **)&d_ca, sizebyte);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to allocate device vector CoeffA (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
    	uint8_t *d_ia = NULL;
    	err = cudaMalloc((void **)&d_ia, sizebyte);
	if (err != cudaSuccess)
	{
        	fprintf(stderr, "Failed to allocate device vector imageA (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	uint8_t *d_ib = NULL;
    	err = cudaMalloc((void **)&d_ib, sizebyte);
	if (err != cudaSuccess)
	{
        	fprintf(stderr, "Failed to allocate device vector imageB (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	 uint8_t *d_id = NULL;
    	err = cudaMalloc((void **)&d_id, sizebyte);
	if (err != cudaSuccess)
	{
        	fprintf(stderr, "Failed to allocate device vector disparity (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}

	err = cudaMalloc((void **)&d_r, 6*sizeof(double));
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to allocate random number vector d_r (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}

	for(int i=0; i<6; i++){
		h_r[i] = (rand() % COEFF_RANGE) - (COEFF_RANGE/2);
	}

    	err = cudaMemcpy(d_r, h_r, 6*sizeof(double), cudaMemcpyHostToDevice);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to copy random numbers (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}

	err = cudaMemcpy(d_ca, h_ca, sizebyte, cudaMemcpyHostToDevice);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to copy vector CA from host to device (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	err = cudaMemcpy(d_ia, h_ia, sizebyte, cudaMemcpyHostToDevice);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to copy vector IA from host to device (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	err = cudaMemcpy(d_ib, h_ib, sizebyte, cudaMemcpyHostToDevice);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to copy vector IB from host to device (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	
	dim3 block_size(B_SIZE,B_SIZE);
  
  unsigned int sizeblocks = (unsigned int)ceil(imgsize/(float)B_SIZE);
	dim3 grid_size (sizeblocks, sizeblocks);
	
  printf("Launching kernel with Block(%d,%d) and Grid(%d,%d)\n", block_size.x, block_size.y, grid_size.x, grid_size.y); 
  
  BlockDisparity* h_bd = (BlockDisparity*)malloc(sizeof(BlockDisparity) * sizeblocks * sizeblocks);
  
  BlockDisparity* d_bd = NULL;
  err = cudaMalloc((void**) &d_bd, sizeof(BlockDisparity) * sizeblocks * sizeblocks);
  if (err != cudaSuccess)
   	{
      fprintf(stderr, "Failed to allocate BlockDisparity array (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    }  
  
  
  disparity_kernel<<<grid_size, block_size>>>(d_ca, d_ia, d_ib,d_id,imgsize, threshold, d_r, d_bd);	

	//int val;	
	//cudaDeviceGetAttribute ( &val,cudaDevAttrMaxGridDimY, 0 );
	//cout << val << " \n"; 
	//cudaDeviceSynchronize (); 
    	err = cudaGetLastError();
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to launch disparity kernel (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}
	err = cudaMemcpy(h_id, d_id, sizebyte, cudaMemcpyDeviceToHost);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to copy vector CA from device to host (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}

	err = cudaMemcpy(h_bd, d_bd, sizeof(BlockDisparity) * sizeblocks * sizeblocks, cudaMemcpyDeviceToHost);
    	if (err != cudaSuccess)
    	{
        	fprintf(stderr, "Failed to copy BlockDisparity array from device to host (error code %s)!\n", cudaGetErrorString(err));
        	exit(EXIT_FAILURE);
    	}

	Mat outputMat(Size(imgsize,imgsize), CV_8U, h_id);
	//cout<<outputMat<<endl;
	imwrite("outImg.jpg", outputMat);	
	cout <<"Done\n";
	return h_bd;
}

/**
 * Host main routine
 */
int main(void)
{
	srand(time(NULL));
	cv::Mat imga, imgb ,  coeffa , coeffb;
	imga = cv::imread("../dsA_0.jpg", CV_LOAD_IMAGE_ANYDEPTH);
	imgb = cv::imread("../dsB_0.jpg", CV_LOAD_IMAGE_ANYDEPTH);
	coeffa = cv::imread("../coeffA_0.jpg", CV_LOAD_IMAGE_ANYDEPTH);
	//coeffb = cv::imread("../coeffB_0.jpg", CV_LOAD_IMAGE_ANYDEPTH);

  unsigned int sizeblocks = (unsigned int)ceil(imga.rows/(float)B_SIZE);

	BlockDisparity* bd = getBlockDisparities(imga,coeffa, imgb);
  
  for(int i=0; i<sizeblocks; i++){
    for(int j=0; j<sizeblocks; j++){
      BlockDisparity disp = bd[i*sizeblocks + j];
      cout<<"i: "<<i<<"j: "<<j<<endl;
      if(disp.filled){
        cout<<disp.a<<" "<<disp.b<<" "<<disp.c<<" "<<disp.d<<" "<<disp.e<<" "<<disp.f<<" "<<endl;
      }else{
        cout<<"Not filled"<<endl;
      }
    }
  }

	return 0;    
}

