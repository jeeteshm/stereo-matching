#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include "constants.hpp"

using namespace cv;
using namespace std;

class AWTCoeffGenerator{
  Mat image;
  Mat awt_prev;
  Mat last_ds; //last performed downsample
  int level, prev_size;

public:
  AWTCoeffGenerator(){
    this->level = NUM_LEVELS;
    this->prev_size = 0;
  }

  void init(Mat image){
    this->image = image;
    Mat coeff;
    getNextAWTCoefficient(coeff);
  }

  void getNextAWTCoefficient(Mat& coeff);

  void getLastDownsample(Mat& ds){
    ds = this->last_ds;
  }
};
