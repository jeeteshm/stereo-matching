#include "stereomatch.hpp"
#include <sstream>
#define NUM_LEVELS 5

const int filter_size = 3;
double filter[3][3] = 
  {
    {0.0625, 0.125, 0.0625},  //1/16, 1/8, 1/16
    {0.125, 0.25, 0.125},     //1/8, 1/4, 1/8
    {0.0625, 0.125, 0.0625}   //1/16, 1/8, 1/16
  };


void generateModel(Mat imageA, Mat imageB, Mat &model){
  //assume images are square
  //assert(imageA.rows == imageB.rows);
  //assert(imageA.cols == imageB.cols);
  assert(imageA.rows == imageA.cols);

  int Size = imageA.rows;
  int brdr = filter_size/2;
  Mat awtA_prev;
  int awtA_prev_size;

  for(int level=NUM_LEVELS; level >= 0; level--){
    cout<<"Level: "<<level<<endl;
    Mat awtA_cur;
    int awtA_cur_size;
    
    if(level!=0){
    
      int step = (1 << level);
      int filter_center = filter_size/2;
      
      awtA_cur_size = (Size >> level) - 2*(brdr-1);
      if((Size % (1 << level)) == 0) awtA_cur_size-=2;
      else awtA_cur_size-=1;
      awtA_cur.create(awtA_cur_size, awtA_cur_size, CV_8U);
      cout<<"awta_cur_size: "<<awtA_cur_size<<endl;
      uint8_t* dataA = (uint8_t*) imageA.data;
      uint8_t* data_awtA_cur = (uint8_t*) awtA_cur.data;

      for(int iA=brdr*step, i=0; iA+brdr*step < Size; iA+= step, i++){
        for(int jA=brdr*step, j=0; jA+brdr*step < Size; jA+= step, j++){  
          /*cout<<"Size "<<Size<<" step "<<step<<" brdr "<<brdr<<" iA: "
            <<iA<<" jA: "<<jA<<" i: "<<i<<" j: "<<j<<endl;*/
          double cur = 0.0;
          for(int fi=0; fi < filter_size; fi++){
            for(int fj=0; fj < filter_size; fj++){
              int mul_iA = iA + step*(fi - filter_center),  
                mul_jA = jA + step*(fj - filter_center);
              cur+= filter[fi][fj]*dataA[mul_iA * Size + mul_jA];
            }
          }

          data_awtA_cur[i*awtA_cur_size + j] = (unsigned char)cur;

        }
        
      } 
    } else {
      awtA_cur = imageA;
      awtA_cur_size = imageA.rows;
    }

    ostringstream filename;
    filename << "awt_" << level << ".jpg";
    cout<<"Writing the AWT for level "<<level<<endl;
    imwrite(filename.str(),awtA_cur);

    if(!awtA_prev.empty()){
      Mat coeff;
      coeff.create(awtA_cur_size, awtA_cur_size, CV_8U);
      uint8_t *cdata = (uint8_t*)coeff.data;
      uint8_t *cur_data = awtA_cur.data, *prev_data = awtA_prev.data;

      //assert(awtA_prev.rows*2 == awtA_cur_size);

      for(int i=0; i<awtA_cur_size; i++){
        for(int j=0; j<awtA_cur_size; j++){
          int diff = prev_data[awtA_prev_size*(i/2) + (j/2)]
            - cur_data[i*awtA_cur_size + j];
          if(diff < 0) diff = -diff;
          cdata[i*awtA_cur_size + j] = diff;
        }
      }

      ostringstream filename;
      filename<<"coeff_"<<level+1<<".jpg";
      cout<<"Writing wavelet coefficients for level "<<level<<endl;
      imwrite(filename.str(), coeff);
    }

    awtA_prev = awtA_cur;
    awtA_prev_size = awtA_cur_size;

  }





}
