#define NUM_LEVELS 5
#define TILL_LEVEL 0
#define B_SIZE 16 //block size
#define W_SIZE 10 //window size
#define STRIDE 100
#define THREADS_PER_BLOCK (B_SIZE*B_SIZE)
#define PIXELS_PER_BLOCK THREADS_PER_BLOCK
#define NUM_POLYNOMIAL_COEFFS 6
#define STEP_SCALE 0.00001
#define HIST_RANGE 70
#define PROFILING true

