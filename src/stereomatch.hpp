#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

void generateModel(Mat imageA, Mat imageB, Mat& model);
void initAWTEngine(Mat imageA);
void getNextAWTCoefficient(Mat imageA, Mat& coeff);
