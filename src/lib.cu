#include "lib.hpp"
#include <time.h>

timespec diff(timespec start, timespec end){
  timespec res;
  if ((end.tv_nsec-start.tv_nsec)<0) {
    res.tv_sec = end.tv_sec-start.tv_sec-1;
    res.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
  } else {
    res.tv_sec = end.tv_sec-start.tv_sec;
    res.tv_nsec = end.tv_nsec-start.tv_nsec;
  }
  return res;
}
