#include "stereomatch.hpp"
#include "awt.hpp"
#include "disparity.hpp"
#include "lib.hpp"
#define AWT_SWITCH false
#define DISPARITY_SWITCH !(AWT_SWITCH)
#define SDEBUG 1

extern char* image_directory;

void generateModel(Mat imageA, Mat imageB, Mat& model){
  //assume images are square
  assert(imageA.rows == imageB.rows);
  assert(imageA.cols == imageB.cols);
  assert(imageA.rows == imageA.cols);

  int Size = imageA.rows >> TILL_LEVEL;
  AWTCoeffGenerator genA, genB;
  genA.init(imageA);
  genB.init(imageB);
  BlockDisparities blck_disp[NUM_LEVELS];

  for(int l=NUM_LEVELS-1; l >= TILL_LEVEL; l--){
    Mat coeffA, coeffB;
    Mat dsA, dsB;
    
    if(AWT_SWITCH){
      timespec timeStart, timeEnd;  
      if(PROFILING){
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeStart);
      }
      
      genA.getNextAWTCoefficient(coeffA);
      
      if(PROFILING){
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
        timespec delta = diff(timeStart, timeEnd);
        cout<<"Last AWT Coeff fetched in "<<delta.tv_sec<<":"
          <<delta.tv_nsec<<" seconds"<<endl;
      }
      
      genA.getLastDownsample(dsA);
    }
    
    
    ostringstream coeff_filenameA, ds_filenameA;
    coeff_filenameA<<image_directory<<"coeffA_"<<l<<".jpg";
    ds_filenameA<<image_directory<<"dsA_"<<l<<".jpg";    
    
    if(DISPARITY_SWITCH){  
      coeffA = imread(coeff_filenameA.str(), CV_LOAD_IMAGE_ANYDEPTH);
      dsA = imread(ds_filenameA.str(), CV_LOAD_IMAGE_ANYDEPTH);    
    }
    
    if(AWT_SWITCH){
      imwrite(coeff_filenameA.str(), coeffA);
      imwrite(ds_filenameA.str(), dsA);
    }
    
    if(AWT_SWITCH){
    
      timespec timeStart, timeEnd;  
      if(PROFILING){
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeStart);
      }
    
      genB.getNextAWTCoefficient(coeffB);
      
      if(PROFILING){
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
        timespec delta = diff(timeStart, timeEnd);
        cout<<"Last AWT Coeff fetched in "<<delta.tv_sec<<":"
          <<delta.tv_nsec<<" seconds"<<endl;
      }
      genB.getLastDownsample(dsB);
    }
    
    ostringstream coeff_filenameB, ds_filenameB;
    coeff_filenameB<<image_directory<<"coeffB_"<<l<<".jpg";
    ds_filenameB<<image_directory<<"dsB_"<<l<<".jpg";    

    if(DISPARITY_SWITCH){      
      coeffB = imread(coeff_filenameB.str(), CV_LOAD_IMAGE_ANYDEPTH);
      dsB = imread(ds_filenameB.str(), CV_LOAD_IMAGE_ANYDEPTH);
    }
    
    if(AWT_SWITCH){
      imwrite(coeff_filenameB.str(), coeffB);
      imwrite(ds_filenameB.str(), dsB);
    }
    
    if(AWT_SWITCH){
      cout<<"Done reading AWT coefficients for level "<<l<<endl;
    }
  
    if(DISPARITY_SWITCH){
      int numBlocks =  (int) ceil( dsA.rows / (float) B_SIZE);
      BlockDisparity* bds = blck_disp[l].bds
        = (BlockDisparity *) malloc( sizeof(BlockDisparity) * numBlocks * numBlocks );

      Mat dispMat;

      timespec timeStart, timeEnd;  
      if(PROFILING){
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeStart);
      }
    
      getBlockDisparities(dsA, coeffA, dsB, bds, dispMat);
      
      if(PROFILING){
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
        timespec delta = diff(timeStart, timeEnd);
        cout<<"Last set of disparity surface fetched in "<<delta.tv_sec<<":"
          <<delta.tv_nsec<<" seconds"<<endl;
      }
      
      ostringstream disp_filename;
      disp_filename<<image_directory<<"disp_"<<l<<".jpg";
      
      imwrite(disp_filename.str(), dispMat);
    
      if(SDEBUG){
        /*
        for(int i=0; i<numBlocks; i++){
          for(int j=0; j<numBlocks; j++){
            BlockDisparity disp = bds[i*numBlocks + j];
            cout<<"i: "<<i<<"j: "<<j<<endl;
            if(disp.filled){
              cout<<disp.a<<" "<<disp.b<<" "<<disp.c<<" "<<disp.d<<" "<<disp.e<<" "<<disp.f<<" "<<disp.last_diff<<endl;
            }else{
              cout<<"Not filled"<<endl;
            }
          }
        }
        */
      }
    }
  }

  if(DISPARITY_SWITCH){
    cout<<"Generating model..."<<endl;

    timespec timeStart, timeEnd;  
    if(PROFILING){
      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeStart);
    }
  
    getDisparity(Size, blck_disp, model);
    
    if(PROFILING){
      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
      timespec delta = diff(timeStart, timeEnd);
      cout<<"Final disparity map generated in "<<delta.tv_sec<<":"
        <<delta.tv_nsec<<" seconds"<<endl;
    }


  }
}
