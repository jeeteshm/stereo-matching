#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "constants.hpp"
#include <iostream>

using namespace std;
using namespace cv;

#define gpuErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

class BlockDisparity{
  //approximates local surface in a block of size BLOCK_SIZE
  //at coordinate (x,y)
  //by polynomial ax2 + by2 + cxy, dx, ey, f
public:
  bool filled;
  double last_diff;
  int x,y;
  double a, b, c, d, e, f;
};

class BlockDisparities{
  //array storing Disparity polynomials
public:
  int size;
  BlockDisparity* bds;
  /*
  BlockDisparities(){
    bds=NULL;
  }

  ~BlockDisparities(){
    if(bds!=NULL){
      //free(bds);
    }
  }
  */
};

void getBlockDisparities(Mat imageA, Mat coeffA, Mat imageB, BlockDisparity* bds, Mat& dispMat);

void getDisparity(int Size, BlockDisparities blck_disp[NUM_LEVELS], Mat &model);
