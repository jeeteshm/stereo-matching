#include "disparity.hpp"
#include <stdio.h>
#include <math.h>
#include <cuda_runtime.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"
//#include "opencv2/gpu/gpu.hpp"
#define COEFF_RANGE 1000
#include <cstdlib>
#include <time.h>
#include <limits.h>
#define DDEBUG 0
#define MODE_FILTERING 1
int interestThreshold, numIterations;

using namespace std;
using namespace cv;

__device__ uint8_t findMinDisp(int row, int col, uint8_t *ia, uint8_t *ib, int imgsize ){
	//int rmin=r;
	int matched_col_B=col;
	unsigned int dmin=INT_MAX, sum;
		
	for(int cB = col-STRIDE ; cB<= col+STRIDE; cB++){
		
    sum=0;
		if(cB<0 || cB>=imgsize){
      continue;
    }
    
		for(int i=row-W_SIZE; i<=row+W_SIZE;i++){
			
      if(i<0 || i>=imgsize){
        continue;
      }
      
			for(int j=cB-W_SIZE; j<=cB+W_SIZE; j++){
        
        if(j<0 || j>=imgsize || j-cB+col <0 || j-cB+col>=imgsize ){
          continue;
        }
					
        sum=sum+abs(ia[i*imgsize+j-cB+col] - ib[i*imgsize+j]);
			}
		}
		
    if(sum<dmin){
      dmin=sum;
      matched_col_B=cB;
    }
		
	}
	return (uint8_t)abs(col-matched_col_B);		
}

__global__ void disparityKernel(uint8_t *ca, uint8_t *ia, uint8_t *ib, uint8_t *id, int imgsize, double* rand, BlockDisparity* bd, int threshold, int iterations ){
  __shared__ int count; //number of interest points
  __shared__ int sh_A[PIXELS_PER_BLOCK][NUM_POLYNOMIAL_COEFFS]; //matrix A
  __shared__ int sh_b[PIXELS_PER_BLOCK]; //vector of disparities
  __shared__ int sh_ATA[NUM_POLYNOMIAL_COEFFS][NUM_POLYNOMIAL_COEFFS]; //matrix A'A
  __shared__ int sh_ATb[NUM_POLYNOMIAL_COEFFS]; //matrix A'Ab
  __shared__ double sh_xk1[NUM_POLYNOMIAL_COEFFS], sh_xk[NUM_POLYNOMIAL_COEFFS]; //x_(k+1) x_k
  __shared__ int sh_HIST[STRIDE];
  __shared__ int peak;
  if(threadIdx.x==0 && threadIdx.y==0){
    count=0;
	peak=0;
  }
	if(threadIdx.x*B_SIZE+threadIdx.y < STRIDE)
	{
		sh_HIST[threadIdx.x*B_SIZE+threadIdx.y]=0;
	}
  __syncthreads();

  int idx=0; //index of this interest point in the matrix A
  int pivot_row = (blockIdx.x*blockDim.x);
  int pivot_col = (blockIdx.y*blockDim.y);
  int row=(blockIdx.x*blockDim.x)+threadIdx.x;
  int col=(blockIdx.y*blockDim.y)+threadIdx.y;
  int i=row*imgsize+col;
  int flag=1;
  if(i<imgsize*imgsize){ 
    if(ca[i] > threshold){//means this is an interest point

      id[i]=findMinDisp(row,col,ia,ib,imgsize);
      if(MODE_FILTERING){
        atomicAdd(&sh_HIST[id[i]],1);
        __syncthreads();
        if(threadIdx.x==0 && threadIdx.y==0){
          for(int l=0;l<STRIDE;l++){
            if(sh_HIST[l] > sh_HIST[peak]){peak =l;}
          }
        }
        __syncthreads();
        if(abs(peak-id[i]) > HIST_RANGE){flag=0;}
      }
      if(flag==1){
        idx=atomicAdd(&count,1);
        sh_A[idx][0]=(row - pivot_row)*(row - pivot_row);
        sh_A[idx][1]=(col - pivot_col)*(col - pivot_col);
        sh_A[idx][2]=(row - pivot_row)*(col - pivot_col);
        sh_A[idx][3]=(row - pivot_row);
        sh_A[idx][4]=(col - pivot_col);
        sh_A[idx][5]=1;
        sh_b[idx]=id[i]; 
      }
      else{
        id[i]=0;
      }
    } else {
      id[i]=0;
    }
  }

  __syncthreads();
    
	if(count == 0){
		bd[gridDim.y * blockIdx.x + blockIdx.y].filled = false;
		return;
	}    
    
  if(threadIdx.x < 6 && threadIdx.y < 6){
      
    sh_ATA[threadIdx.x][threadIdx.y] = 0;
    for(int i = 0; i < count; i++){
      sh_ATA[threadIdx.x][threadIdx.y] += sh_A[i][threadIdx.x] * sh_A[i][threadIdx.y];
    }
  }
	
  if(threadIdx.x < 6 && threadIdx.y == 0){
    sh_ATb[threadIdx.x] = 0;
    for(int i=0; i<count; i++){
      sh_ATb[threadIdx.x] += sh_A[i][threadIdx.x] * sh_b[i];
    }
  }

  __syncthreads();

  if(threadIdx.x == 0 && threadIdx.y == 0){
    for(int i=0; i<6; i++){
      sh_xk[i] = 0; //rand[i]; //initialize with random values
    }
	
    int it = 1; //num of iterations
    double last_diff = 0;
    do{
	  last_diff = 0;
	  
      for(int i=0; i<6; i++){
        //x_k+1 = -A'b
        sh_xk1[i] = -sh_ATb[i];
        for(int j=0; j<6; j++){
          sh_xk1[i] += sh_ATA[i][j] * sh_xk[j];
        }
        
        //x_k+1 = \gamma (A'Ax - A'b)
        sh_xk1[i] *= STEP_SCALE / ((double)(it));
        
        last_diff += sh_xk1[i] * sh_xk1[i];
        
        //x_k+1 = x_k - \gamma (A'Ax - A'b)
        sh_xk1[i] = sh_xk[i] - sh_xk1[i];

      }
      
      for(int j=0; j<6; j++){
        sh_xk[j] = sh_xk1[j];
      }
      
      it++;
    }while(it < iterations);

    bd[gridDim.y * blockIdx.x + blockIdx.y].filled = true;
    bd[gridDim.y * blockIdx.x + blockIdx.y].a = sh_xk1[0];
    bd[gridDim.y * blockIdx.x + blockIdx.y].b = sh_xk1[1];
    bd[gridDim.y * blockIdx.x + blockIdx.y].c = sh_xk1[2];
    bd[gridDim.y * blockIdx.x + blockIdx.y].d = sh_xk1[3];
    bd[gridDim.y * blockIdx.x + blockIdx.y].e = sh_xk1[4];
    bd[gridDim.y * blockIdx.x + blockIdx.y].f = sh_xk1[5];   
    bd[gridDim.y * blockIdx.x + blockIdx.y].last_diff = last_diff;   
  }
}

void getBlockDisparities(Mat imageA, Mat coeffA, Mat imageB, BlockDisparity* bds, Mat& dispMat){
	
  //gpuErrorCheck(cudaSetDevice(3));
  srand(time(NULL));
	int imgsize=(imageA.rows);
	size_t sizebyte = imgsize*imgsize*sizeof(uint8_t);
	uint8_t* h_ca = (uint8_t*) coeffA.data;
	uint8_t* h_ia = (uint8_t*) imageA.data;
	uint8_t* h_ib = (uint8_t*) imageB.data;
	uint8_t* h_id = (uint8_t*) malloc(sizebyte); //disparity values
  
  uint8_t *d_ca = NULL;
	gpuErrorCheck(cudaMalloc((void **)&d_ca, sizebyte));

 	uint8_t *d_ia = NULL;
 	gpuErrorCheck(cudaMalloc((void **)&d_ia, sizebyte));

	uint8_t *d_ib = NULL;
	gpuErrorCheck(cudaMalloc((void **)&d_ib, sizebyte));

  uint8_t *d_id = NULL;
  gpuErrorCheck(cudaMalloc((void **)&d_id, sizebyte));

	double h_r[NUM_POLYNOMIAL_COEFFS], *d_r = NULL; //random seed values for host and device
	gpuErrorCheck(cudaMalloc((void **)&d_r, NUM_POLYNOMIAL_COEFFS*sizeof(double)));

	for(int i=0; i<NUM_POLYNOMIAL_COEFFS; i++){
		h_r[i] = (rand() % COEFF_RANGE);
    //cout<<"RANDOM "<<h_r[i]<<endl;
	}

 	gpuErrorCheck(cudaMemcpy(d_r, h_r, NUM_POLYNOMIAL_COEFFS*sizeof(double), cudaMemcpyHostToDevice));

	gpuErrorCheck(cudaMemcpy(d_ca, h_ca, sizebyte, cudaMemcpyHostToDevice));

	gpuErrorCheck(cudaMemcpy(d_ia, h_ia, sizebyte, cudaMemcpyHostToDevice));

	gpuErrorCheck(cudaMemcpy(d_ib, h_ib, sizebyte, cudaMemcpyHostToDevice));
	
	dim3 block_size(B_SIZE,B_SIZE);
  unsigned int sizeblocks = (unsigned int)ceil(imgsize/(double)B_SIZE);
	dim3 grid_size (sizeblocks, sizeblocks);
	  
  BlockDisparity* h_bd = bds;
  
  BlockDisparity* d_bd = NULL;
  gpuErrorCheck(cudaMalloc((void**) &d_bd, sizeof(BlockDisparity) * sizeblocks * sizeblocks));
 
  printf("Launching kernel with Block(%d,%d) and Grid(%d,%d)\n", block_size.x, block_size.y, grid_size.x, grid_size.y); 
 
  disparityKernel<<<grid_size, block_size>>>(d_ca, d_ia, d_ib, d_id, imgsize, d_r, d_bd, interestThreshold, numIterations);	

  gpuErrorCheck(cudaGetLastError());
  
	gpuErrorCheck(cudaMemcpy(h_id, d_id, sizebyte, cudaMemcpyDeviceToHost));

	gpuErrorCheck(cudaMemcpy(h_bd, d_bd, sizeof(BlockDisparity) * sizeblocks * sizeblocks, cudaMemcpyDeviceToHost));

	dispMat = Mat(Size(imgsize,imgsize), CV_8U, h_id);
	cout <<"Done\n";
}

double calcDisparity(double x, double y, BlockDisparity bd){
  double res = 0.0;
  res += bd.a * x * x;
  res += bd.b * y * y;
  res += bd.c * x * y;
  res += bd.d * x;
  res += bd.e * y;
  res += bd.f;
  return res;
}

void getDisparity(int Size, BlockDisparities* blck_disp,
                  Mat& model){
  model.create(Size, Size, CV_8U);
  uint8_t* mdata = (uint8_t*) model.data;

  int from = NUM_LEVELS-1, to = TILL_LEVEL;
  for(int level= from; level>=to; level--){
 
    if(DDEBUG){
      cout<<"Generating model at level "<<level<<endl;
    }
    BlockDisparities bdisps = blck_disp[level];
    BlockDisparity* bds = bdisps.bds;
    int ds_size = Size >> (level - TILL_LEVEL);
    int numBlocks =  (int) ceil( ds_size / (float) B_SIZE);

    if(DDEBUG){
      cout<<"ds_size "<<ds_size<<" numBlocks "<<numBlocks<<endl;
    }
    
    for(int ib=0; ib<numBlocks; ib++){
      for(int jb=0; jb<numBlocks; jb++){
        
        if(DDEBUG){
          cout<<"ib "<<ib<<" jb "<<jb<<endl;
        }
        
        BlockDisparity bd = bds[ib*numBlocks + jb];
        if(bd.filled == false){
          if(DDEBUG){
            cout<<"this block not filled"<<endl;
          }
          continue;
        }
          
        int x_bgn = (ib*B_SIZE) << (level - TILL_LEVEL),
          y_bgn = (jb*B_SIZE) << (level - TILL_LEVEL),
          x_end = (ib*B_SIZE + B_SIZE) << (level - TILL_LEVEL),
          y_end = (jb*B_SIZE + B_SIZE) << (level - TILL_LEVEL);

        if(DDEBUG){
          cout<<"x_bgn "<<x_bgn<<" x_end "<<x_end<<" y_bgn "<<y_bgn
            <<" y_end "<<y_end<<endl;
        }
	double max_disp = 0;
        for(int x = x_bgn; x < x_end; x ++){
          for(int y = y_bgn; y < y_end; y ++){
            if(x >= Size || y >= Size) continue;
            double xs
              = ((double)(x - x_bgn)) / ((double)(1 << (level - TILL_LEVEL)));
            double ys
              = ((double)(y - y_bgn)) / ((double)(1 << (level - TILL_LEVEL)));
            
            double ds = calcDisparity(xs, ys, bd);
            double disp = ds * (1 << (level - TILL_LEVEL));
            
            int max_disp = 2000 >> TILL_LEVEL;
            if(disp > max_disp) disp = max_disp;
            if(disp < 0) disp = 0;
            
            mdata[x*Size + y] = (uint8_t) floor(disp*255.0/(double)max_disp);
            //mdata[x*Size + y] = 255;
          }
        }
      }
    }
  }
}
