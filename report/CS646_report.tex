\documentclass[4pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm2e}
\usepackage[margin=1.5 cm]{geometry}
\usepackage{graphicx}
\newcommand{\Heading}[1]{\large{\textsc{#1}}}

\newcommand{\jagah}{\vspace{0.05cm}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\U}{\mathcal{U}}
\newcommand{\V}{\mathcal{V}}
\newcommand{\W}{\mathcal{W}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\Rn}{{\mathbb{R}^n}}
\newcommand{\inv}{^{-1}}

\begin{document}
\begin{center}  
  {CS646}\\
  {Parallel Algorithms}\\
  {Paper 4}\\
  {``Robust Stereo Image Matching for Spaceborne Imagery"}\\
  \Large{REPORT}\\
  \hrulefill
  \normalsize
  %~ \begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}} l r}
  \begin{flushright}
    10161, Ashu Gupta\\
    10314, Jeetesh Mangwani\\
  \end{flushright}
  %~ \end{tabular*}
\end{center}
\normalsize
\jagah

\Heading{Objective}\\\\
To approximate disparity map for the given pairs of stereoimages using 5-level Atrous Wavelet Transform (AWT) Algorithm \& Least Squares Method for Polynomial Fitting\\

\Heading{Workflow, implementation \& time complexity}\\
\begin{itemize}
  \item \textbf{Decompose the AFT-FORE stereo image pair using the ``$\acute{a}\ trous$'' wavelet decomposition algorithm}\\
    \begin{itemize}
      \item Construct 5-level image pyramid by successive application of the low-pass filters, $h_j$, followed by calculation of wavelet coefficients, $w_j$ for each scale $1 \leq j \leq 5$:\\
        $h_j = \left(
          \begin{array}{ccc}
            \frac1{16}\ ...0\ 0\ 0_{j\ times}... & \frac1{8}\ ...0\ 0\ 0_{j\ times}... & \frac1{16}\\
            . & . & .\\
            . & . & .\\
            0_{j\ times} & 0_{j\ times} & 0\\
            . & . & .\\
            . & . & .\\
            \frac1{8}\ ...0\ 0\ 0_{j\ times}...  & \frac1{4}\ ...0\ 0\ 0_{j\ times}...  & \frac1{8}\\
            . & . & .\\
            . & . & .\\
            0_{j\ times} & 0_{j\ times} & 0\\
            . & . & .\\
            . & . & .\\
            \frac1{16}\ ...0\ 0\ 0_{j\ times}...  & \frac1{8}\ ...0\ 0\ 0_{j\ times}...  & \frac1{16}\\
          \end{array}
        \right)$\\\\\\
        $c_{j+1} = h_j \bullet c_j$, where $\bullet$ represents application of the filter $h_j$ to $c_j$\\
        $w_j+1 = c_{j+1} - c_j$, so that $c_0 = c_N + \sum_{j=1}^{N} w_j$\\
        This step has been parallelized using the inbuilt gpu function "filter2D". Since it is plain convolution with the filter illustrated above, it is an $O(1)$ step\\
        
    \end{itemize}
  \item \textbf{Extract feature points using wavelet coefficients}\\
    We filter out those points whose wavelet coefficient is above the "threshold" (an input parameter). We call these points as interest points.\\
    
    This step is done in parallel, one thread per pixel; hence is $O(1)$\\
    
  \item \textbf{Matching those points in the partner image and generating disparity map}\\
    No parallax variation is expected in the across-track direction. Hence, for each interest point in the AFT image, corresponding points is searched for in the FORE image using "sum of absolute differences in pixel intensities" metric as follows: the search window in the FORE image is shifted in y direction and similarity metric calculated for each shift; the center of the target chip with highest similarity coefficient is identified as the match for this interest point. Disparity is the difference between the coordinates of these matched pairs.
    
    This step has been parallelised using block sizes (say BxB) of $16\times16$ pixels.\\
    
    Let $W$ be the window size and $M$ be the maximum disparity. Then, this step is $O(MW^2)$\\
    
    Due to time constraints, we've dropped the aspect based correlation.\\
    
  \item \textbf{Execute mode filtering and outlier removal}\\
    The image area is divided into blocks of size $16 \times 16$. For each such block, a histogram of disparity of $N$ interest points of that block, $d_1, d_2, .... d_N$ is constructed. Interest points with disparity close to the mode of the histogram are retained.
    
    This step has also been parallelized. Each thread registers the disparity it observed by atomically adding to a counter. Since there can be atmost $B^2$ additions to any counter, this step is $O(B^2)$\\
    
  \item \textbf{Generate local surface using interpolation} \\
    Disparity for each block is modelled as a local 2D surface. The disparities obtained are fit into a second-degree polynomial, which gives six coefficients that are used to get the complete mapping of that block. Method of least squares using gradient descent method is used here.\\
    
    This step has been partially parallelized (atmost 36 threads are used for simplicity).\\
    
    Time complexity of this step is discussed in the next subsection.\\
    
  \item \textbf{Modelling the problem as Least Squares Problem} \\
    Our polynomial surface model: $ax^2+by^2+cxy+dx+ey+f$\\
    Objective function to be minimised: $\|Ax-b\|^2$\\
    $A_{ij} = p_{ij}$, where $p_i = (x_i^2, y_i^2, x_iy_i, x_i, y_i, 1)$ corresponds to the $i^{th}$ interest point $(x_i, y_i)$\\

    $b_i$ is the disparity of the $i^{th}$ point\\
    $x = (a, b, c, d, e, f)$ is the variable vector\\
    
    Using gradient descent method and step function as $\frac1{t}$, where $t\in[1,N]$ is the iteration number and $N$ is the number of iterations (an input parameter),\\
    
    $x_{k+1} = x_k - c \frac1{t} \triangledown \|Ax-b\|^2 = x_k - 2c \frac1{t} A'(Ax-b)$
    $= x_k - \gamma \frac1{t} A'(Ax-b)$\\
    
    This step involves calculating the 36 entries of $A^TA$ in parallel by 36 threads, which takes $O(B^2)$ steps, where $B=16$ is the block size (an input parameter). Similarly, $A^Tb$ takes $O(B)$ steps.\\
    
    Each gradient descent step involves sequentially calculating $A^TAx$, which is an $O(B^2)$ step. Thus, this takes overall $O(N(B^2+ B))= O(NB^2)$ steps\\
    
    
  \item \textbf{Refine match in higher resolutions}\\
    This process is continued until the original resolution image is obtained. At each level, outlier rejection is performed.\\

  \item \textbf{Input Parameters}
  \begin{itemize}
    \item Window Size
    \item Maximum Disparity
    \item Interest Threshold
    \item No. of iterations in Least Squares Method
    \item Gamma (constant factor in the linear least squares problem)
  \end{itemize}

\end{itemize}  

\Heading{Results}\\
\begin{center}
  3D disparity map plot for images 21.tif (FORE) and 22.tif (AFT) [see below]\\
  \includegraphics[width=20cm]{disp01.jpg}\\
  Image 21.tif [see below]\\
  \includegraphics[width=12cm]{21.png}\\
  3D disparity map plot for images 43.tif (FORE) and 44.tif (AFT) [see below]\\
  \includegraphics[width=20cm]{disp03.jpg}\\
  Image 43.tif [see below]\\
  \includegraphics[width=12cm]{43.png}
\end{center}


\Heading{Configuration}\\
\begin{itemize}
\item 
nvcc: NVIDIA (R) Cuda compiler driver\\
Copyright (c) 2005-2013 NVIDIA Corporation\\
Cuda compilation tools, release 5.5, V5.5.0

\item
Computer compatibility = 1.3 or higher
\item
OpenCV v2.4.6.1
\item
Tesla T10 Processor
\item
Grid sizes: 8x8, 16x16, 32x32, 64x64, 128x128 (for each pyramid level)\\
Block size: 16x16

\end{itemize}
\end{document}
